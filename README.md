# MONSTAR Core - Elasticsearch
[![Docker Repository on Quay](https://quay.io/repository/profidata/monstar.elasticsearch/status "Docker Repository on Quay")](https://quay.io/repository/profidata/monstar.elasticsearch)
This repository provides a lightly customised version of Elasticsearch that is one of two MONSTAR core components.

## Getting Started

### Prerequisites
- a container runtime for building and running container images
- [Set vm.max_map_count to at least 262144](https://www.elastic.co/guide/en/elasticsearch/reference/master/docker.html#_set_vm_max_map_count_to_at_least_262144):
  ```bash
  sudo sysctl -w vm.max_map_count=262144
  ```

## Building the image
```bash
buildah bud --format docker -t localhost/monstar.elasticsearch:latest
```

### Running
```bash
podman run -d -p '9200:9200' \
    -e ELASTIC_CLUSTER_NAME=monstar-cluster \
    -v elasticsearch-data:/usr/share/elasticsearch/data \
    localhost/monstar.elasticsearch
```

## Details

### Environment variables
The following environment variables configure this service:

| Variable Name | Description | Status |
|---|---|---|
| `ELASTIC_CLUSTER_NAME` | Name of the Elasticsearch cluster | required |

### Swap and memory limit
Swapping must be disabled for the Elasticsearch service. To achieve that, set the flags `--memory` and `memory-swap` to the same value, e.g. 4g. This will limit the container memory to the given value with no swap memory allocated. Elasticsearch will automatically configure the JVM heap space to be half of that limit.

Should the above still lead to swapping, consult [this page](https://www.elastic.co/guide/en/elasticsearch/reference/current/setup-configuration-memory.html) for further measures that can be taken.

### Nofile
Increasse the ulimit for nofile of the user running the Elasticsearech container to at least 65535. Configure the container to adopt thoise settings by adding the option `--ulimit host`

### Running it with MONSTAR
Please consult the [core](https://gitlab.com/profidata-ag/monstar/core) repository for usage examples.

## Acknowledgments
* This image is based on [Elasticsearch](https://www.elastic.co/elasticsearch)

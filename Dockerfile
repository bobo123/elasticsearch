ARG ELK_VERSION=7.12.1

FROM docker.elastic.co/elasticsearch/elasticsearch:${ELK_VERSION}

# Add configuration files
COPY config/elasticsearch.yml /usr/share/elasticsearch/config/elasticsearch.yml
